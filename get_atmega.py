import serial
import time
import sqlite3
import json
import urllib2
from time import gmtime, strftime

data = {
        "sensor_value": {
                "value": 0,
                "sensor_name": "sensor_1",
                "region_name": "geladeira",
                "region_lat": "-23.611771",
                "region_lng": "-46.697565",
                "date": "2016-06-20T06:37:52.000Z"
        }
}


ard = serial.Serial('/dev/tty96B0',9600);

if __name__ == '__main__':
	print "E-BillBreaker System";
	while True:
		value = ard.readline();
		#print "Potencia = ", value
		data["sensor_value"]["value"] = value
		print data["sensor_value"]["value"]
		data["sensor_value"]["date"] = strftime("%Y-%m-%dT%H:%M:%S.000Z", gmtime())
		print data["sensor_value"]["date"]
		req = urllib2.Request('https://edx-ruby-rails-rodrigoadfaria-1.c9users.io/register')
		req.add_header('Content-Type', 'application/json')
		a = json.dumps(data)
		try:
			response = urllib2.urlopen(req,a)
		except Exception as e:
			print "Erro ao postar no banco de dados"
